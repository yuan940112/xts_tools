// @ts-nocheck
/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AttrsManager } from '../model/AttrsManager';
import Utils from '../model/Utils'
import { it, afterEach } from '@ohos/hypium';
import windowSnap from '../model/snapShot';
import Settings from '../model/Settings';
import Logger from '../model/Logger'
import router from '@ohos.router';
import {
  Driver,
  ON,
} from '@ohos.UiTest';

export default class CommonTest {
  static initTest(pageConfig, supportView, testValues) {

    afterEach(async function (done) {
      if (Settings.windowClass == null) {
        return
      }

      Settings.windowClass.destroyWindow((err) => {
        if (err.code) {
          Logger.error('TEST', `Failed to destroy the window. Cause : ${JSON.stringify(err)}`)
          return;
        }
        Logger.info('TEST', `Succeeded in destroy the window.`);
      })
      await Utils.sleep(1000);
      done()
    })

    //根据要测试的组件和属性值循环创建case
    supportView.forEach(targetView => {
      testValues.forEach(testValue => {
        let caseTag = pageConfig.testName + "_" + targetView + "_" + testValue.describe
        //create cases
        it(caseTag, 0, async (done) => {
          pageConfig['targetView'] = targetView;
          pageConfig['componentKey'] = targetView;

          //创建窗口，设置窗口页面
          Settings.createWindow(pageConfig.pageUrl)
          console.info('FocusTest Settings.createWindow success');

          await Utils.sleep(1000);

          //跳转页面并传递参数
          await router.pushUrl({
            url: pageConfig.pageUrl,
            params: {
              view: pageConfig
            }
          })
          console.info('FocusTest router.pushUrl success');
          //在此处可以获取页面中的组件进行一些操作，例如获取组件并点击
          // await Utils.clickComponentByKey(targetView)

          await Utils.sleep(1000);
          console.info('FocusTest AttrsManager.change begin');
          //更改属性值
          AttrsManager.change(caseTag, testValue.setValue);
          //截图
          await Utils.sleep(1000);
          console.info('FocusTest windowSnap.snapShot begin');
          await windowSnap.snapShot(globalThis.context, caseTag)
          console.info('FocusTest windowSnap.snapShot success');
          await Utils.sleep(1000);
          done()
        });
      })
    });
  }

  static initTest_matrix(pageConfig, supportView, testValues, id) {

    afterEach(async function (done) {
      if (Settings.windowClass == null) {
        return
      }

      Settings.windowClass.destroyWindow((err) => {
        if (err.code) {
          Logger.error('TEST', `Failed to destroy the window. Cause : ${JSON.stringify(err)}`)
          return;
        }
        Logger.info('TEST', `Succeeded in destroy the window.`);
      })
      await Utils.sleep(1000);
      done()
    })

    //根据要测试的组件和属性值循环创建case
    supportView.forEach(targetView => {
      testValues.forEach(testValue => {
        let caseTag = pageConfig.testName + "_" + targetView + "_" + testValue.describe
        //create cases
        it(caseTag, 0, async (done) => {
          pageConfig['targetView'] = targetView;
          pageConfig['componentKey'] = targetView;

          //创建窗口，设置窗口页面
          Settings.createWindow(pageConfig.pageUrl)
          console.info('FocusTest Settings.createWindow success');

          await Utils.sleep(1000);

          //跳转页面并传递参数
          await router.pushUrl({
            url: pageConfig.pageUrl,
            params: {
              view: pageConfig
            }
          })
          console.info('FocusTest router.pushUrl success');
          //在此处可以获取页面中的组件进行一些操作，例如获取组件并点击
          // await Utils.clickComponentByKey(targetView)

          await Utils.sleep(1000);
          console.info('FocusTest AttrsManager.change begin');
          //更改属性值
          AttrsManager.change(caseTag, testValue.setValue);
          let driver = Driver.create();
          let component = await driver.findComponent(ON.id(id));
          let rect = await component.getBounds();
          await driver.click(rect.left + Math.floor((rect.right - rect.left) / 2), rect.top + Math.floor((rect.bottom
          - rect.top) / 2));
          //截图
          await Utils.sleep(1000);
          console.info('FocusTest windowSnap.snapShot begin');
          await windowSnap.snapShot(globalThis.context, caseTag)
          console.info('FocusTest windowSnap.snapShot success');
          await Utils.sleep(1000);
          done()
        });
      })
    });
  }

  static initTest_touchEvent(pageConfig, supportView, testValues, id) {

    afterEach(async function (done) {
      if (Settings.windowClass == null) {
        return
      }

      Settings.windowClass.destroyWindow((err) => {
        if (err.code) {
          Logger.error('TEST', `Failed to destroy the window. Cause : ${JSON.stringify(err)}`)
          return;
        }
        Logger.info('TEST', `Succeeded in destroy the window.`);
      })
      await Utils.sleep(1000);
      done()
    })

    //根据要测试的组件和属性值循环创建case
    supportView.forEach(targetView => {
      testValues.forEach(testValue => {
        let caseTag = pageConfig.testName + "_" + targetView + "_" + testValue.describe
        //create cases
        it(caseTag, 0, async (done) => {
          pageConfig['targetView'] = targetView;
          pageConfig['componentKey'] = targetView;

          //创建窗口，设置窗口页面
          Settings.createWindow(pageConfig.pageUrl)
          console.info('FocusTest Settings.createWindow success');

          await Utils.sleep(1000);

          //跳转页面并传递参数
          await router.pushUrl({
            url: pageConfig.pageUrl,
            params: {
              view: pageConfig
            }
          })
          console.info('FocusTest router.pushUrl success');
          //在此处可以获取页面中的组件进行一些操作，例如获取组件并点击
          // await Utils.clickComponentByKey(targetView)
          await Utils.sleep(1000);
          console.info('FocusTest AttrsManager.change begin');
          let driver = Driver.create();
          let component = await driver.findComponent(ON.id(id));
          let rect = await component.getBounds();
          await driver.click(rect.left + Math.floor((rect.right - rect.left) / 2), rect.top + Math.floor((rect.bottom - rect.top) / 2));
          //截图
          //更改属性值
          AttrsManager.change(caseTag, testValue.setValue);
          await Utils.sleep(1000);
          console.info('FocusTest windowSnap.snapShot begin');
          await windowSnap.snapShot(globalThis.context, caseTag)
          console.info('FocusTest windowSnap.snapShot success');
          await Utils.sleep(1000);
          done()
        });
      })
    });
  }

  static initTest1(pageConfig, supportView, testValues) {

    afterEach(async function (done) {
      if (Settings.windowClass == null) {
        return
      }

      Settings.windowClass.destroyWindow((err) => {
        if (err.code) {
          Logger.error('TEST', `Failed to destroy the window. Cause : ${JSON.stringify(err)}`)
          return;
        }
        Logger.info('TEST', `Succeeded in destroy the window.`);
      })
      await Utils.sleep(1000);
      done()
    })

    //根据要测试的组件和属性值循环创建case
    supportView.forEach(targetView => {
      testValues.forEach(testValue => {
        let caseTag = pageConfig.testName + "_" + targetView + "_" + testValue.describe
        //create cases
        it(caseTag, 0, async (done) => {
          pageConfig['targetView'] = targetView;
          pageConfig['componentKey'] = targetView;

          //创建窗口，设置窗口页面
          Settings.createWindow(pageConfig.pageUrl,{X:100,Y:100,width:600,height:1250,dpi:200})

          await Utils.sleep(1000);

          //跳转页面并传递参数
          await router.pushUrl({
            url: pageConfig.pageUrl,
            params: {
              view: pageConfig
            }
          })
          await Utils.sleep(1000);

          //更改属性值
          AttrsManager.change(caseTag, testValue.setValue);

          //截图
          await Utils.sleep(2000);
          windowSnap.snapShot(globalThis.context, caseTag)
          await Utils.sleep(1000);

          done()
        });
      })
    });
  }

  static initTest_response(pageConfig, supportView, testValues) {

    afterEach(async function (done) {
      if (Settings.windowClass == null) {
        return
      }

      Settings.windowClass.destroyWindow((err) => {
        if (err.code) {
          Logger.error('TEST', `Failed to destroy the window. Cause : ${JSON.stringify(err)}`)
          return;
        }
        Logger.info('TEST', `Succeeded in destroy the window.`);
      })
      await Utils.sleep(1000);
      done()
    })

    //根据要测试的组件和属性值循环创建case
    supportView.forEach(targetView => {
      testValues.forEach(testValue => {
        let caseTag = pageConfig.testName + "_" + targetView + "_" + testValue.describe
        //create cases
        it(caseTag, 0, async (done) => {
          pageConfig['targetView'] = targetView;
          pageConfig['componentKey'] = targetView;

          //创建窗口，设置窗口页面
          Settings.createWindow(pageConfig.pageUrl,{X:100,Y:100,width:600,height:1250,dpi:200})
          await Utils.sleep(1000);

          //跳转页面并传递参数
          await router.pushUrl({
            url: pageConfig.pageUrl,
            params: {
              view: pageConfig
            }
          })
          await Utils.sleep(1000);

          let driver = Driver.create();
          let component = await driver.findComponent(ON.id("Me"));
          let rect = await component.getBounds();
          await driver.click(rect.left+testValue.setValue.x, rect.top+testValue.setValue.y);
          await Utils.sleep(1000);

          windowSnap.snapShot(globalThis.context, caseTag)
          await Utils.sleep(1000);

          done()
        });
      })
    });

  }

  static initTest_clickNone(pageConfig, supportView, testValues) {
    afterEach(async function (done) {
      if (Settings.windowClass == null) {
        return
      }

      Settings.windowClass.destroyWindow((err) => {
        if (err.code) {
          Logger.error('TEST', `Failed to destroy the window. Cause : ${JSON.stringify(err)}`)
          return;
        }
        Logger.info('TEST', `Succeeded in destroy the window.`);
      })
      await Utils.sleep(1000);
      done()
    })

    //根据要测试的组件和属性值循环创建case
    supportView.forEach(targetView => {
      testValues.forEach(testValue => {
        let caseTag = pageConfig.testName + "_" + targetView + "_" + testValue.describe
        //create cases
        it(caseTag, 0, async (done) => {
          pageConfig['targetView'] = targetView;
          pageConfig['componentKey'] = targetView;

          //创建窗口，设置窗口页面
          Settings.createWindow(pageConfig.pageUrl,{X:100,Y:100,width:600,height:1250,dpi:200})
          await Utils.sleep(1000);

          //跳转页面并传递参数
          await router.pushUrl({
            url: pageConfig.pageUrl,
            params: {
              view: pageConfig
            }
          })
          await Utils.sleep(1000);

          windowSnap.snapShot(globalThis.context, caseTag)
          await Utils.sleep(1000);

          done()
        });
      })
    });

  }

  static initTest_click(pageConfig, supportView, testValues) {
    afterEach(async function (done) {
      if (Settings.windowClass == null) {
        return
      }

      Settings.windowClass.destroyWindow((err) => {
        if (err.code) {
          Logger.error('TEST', `Failed to destroy the window. Cause : ${JSON.stringify(err)}`)
          return;
        }
        Logger.info('TEST', `Succeeded in destroy the window.`);
      })
      await Utils.sleep(1000);
      done()
    })

    //根据要测试的组件和属性值循环创建case
    supportView.forEach(targetView => {
      testValues.forEach(testValue => {
        let caseTag = pageConfig.testName + "_" + targetView + "_" + testValue.describe
        //create cases
        it(caseTag, 0, async (done) => {
          pageConfig['targetView'] = targetView;
          pageConfig['componentKey'] = targetView;

          //创建窗口，设置窗口页面
          Settings.createWindow(pageConfig.pageUrl,{X:100,Y:100,width:600,height:1250,dpi:200})
          await Utils.sleep(1000);

          //跳转页面并传递参数
          await router.pushUrl({
            url: pageConfig.pageUrl,
            params: {
              view: pageConfig
            }
          })
          await Utils.sleep(1000);

          await Utils.clickComponentByKey(targetView)
          await Utils.sleep(1000);

          windowSnap.snapShot(globalThis.context, caseTag)
          await Utils.sleep(1000);

          done()
        });
      })
    });

  }

  static initTest_click1(pageConfig, supportView, testValues) {

    afterEach(async function (done) {
      if (Settings.windowClass == null) {
        return
      }

      Settings.windowClass.destroyWindow((err) => {
        if (err.code) {
          Logger.error('TEST', `Failed to destroy the window. Cause : ${JSON.stringify(err)}`)
          return;
        }
        Logger.info('TEST', `Succeeded in destroy the window.`);
      })
      await Utils.sleep(1000);
      done()
    })

    //根据要测试的组件和属性值循环创建case
    supportView.forEach(targetView => {
      testValues.forEach(testValue => {
        let caseTag = pageConfig.testName + "_" + targetView + "_" + testValue.describe
        //create cases
        it(caseTag, 0, async (done) => {
          pageConfig['targetView'] = targetView;
          pageConfig['componentKey'] = targetView;

          //创建窗口，设置窗口页面
          Settings.createWindow(pageConfig.pageUrl,{X:100,Y:100,width:600,height:1250,dpi:200})
          await Utils.sleep(1000);

          //跳转页面并传递参数
          await router.pushUrl({
            url: pageConfig.pageUrl,
            params: {
              view: pageConfig
            }
          })
          await Utils.sleep(1000);

          let driver = Driver.create();
          let component = await driver.findComponent(ON.id("Me"));
          let rect = await component.getBounds();
          await driver.click(rect.left+testValue.setValue.x, rect.top+testValue.setValue.y);
          await Utils.sleep(1000);

          windowSnap.snapShot(globalThis.context, caseTag)
          await Utils.sleep(1000);

          done()
        });
      })
    });
  }

}